package network

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.coroutines.awaitObject
import util.retry
import java.net.URI

class SimpleNetworker(
    private val fuelManager: FuelManager,
    responsePrinter: ResponsePrinter,
) {

    init {
        fuelManager.apply {
            timeoutInMillisecond = 15_000
            //addResponseInterceptor { responsePrinter }
        }
    }

    var basePath: URI? = fuelManager.basePath?.let { URI.create(it) }
        set(value) {
            fuelManager.basePath = value.toString()
            field = value
        }

    var baseHeaders: Map<String, String>? = fuelManager.baseHeaders
        set(value) {
            fuelManager.baseHeaders = value
            field = value
        }

    suspend inline fun <reified T> getObject(path: String): T =
        retry(times = 20) { Fuel.get(path).awaitObject(kotlinxDeserializerOf()) }

    suspend inline fun <reified T> getList(path: String): List<T> =
        retry(times = 20) { Fuel.get(path).awaitObject(kotlinxListDeserializerOf()) }

    suspend inline fun <reified T> getFullPaginatedList(
        path: String,
        queryKey: String,
        startCount: Int = 1
    ): List<T> {
        var count = startCount
        val getListAction = suspend { getList<T>("$path?$queryKey=$count") }
        var latestList = getListAction.invoke()
        val totalList = latestList.toMutableList()
        while (latestList.isNotEmpty()) {
            latestList = getListAction.invoke()
            totalList += latestList
            count++
        }
        return totalList.toList()
    }
}
