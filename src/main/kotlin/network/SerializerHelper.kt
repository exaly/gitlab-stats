package network

import com.github.kittinunf.fuel.serialization.kotlinxDeserializerOf
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

@OptIn(InternalSerializationApi::class)
inline fun <reified T : Any> kotlinxListDeserializerOf(
    json: Json = SerializerHelper.jsonConfig
) = kotlinxDeserializerOf(ListSerializer(T::class.serializer()), json)

@OptIn(InternalSerializationApi::class)
inline fun <reified T : Any> kotlinxDeserializerOf(
    json: Json = SerializerHelper.jsonConfig
) = kotlinxDeserializerOf(T::class.serializer(), json)

object SerializerHelper {
    val jsonConfig = Json {
        allowStructuredMapKeys = true
        ignoreUnknownKeys = true
        coerceInputValues = true
    }
}
