package network

import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.ResponseTransformer

class ResponsePrinter : ResponseTransformer {

    override fun invoke(p1: Request, p2: Response): Response {
        println(String(p2.body().toByteArray()))
        return p2
    }
}