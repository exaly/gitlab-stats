package service

import data.PipelineData
import data.PipelineRepository
import data.Status

class SuccessfulPipelinesPercentageService(
    private val pipelineRepository: PipelineRepository
) {

    suspend fun calc() = pipelineRepository.getPipelines()
        .groupBy { it.yearWeekNumber }
        .map {
            StatsData(
                it.key.year,
                it.key.weekNumber,
                it.value.calcSuccessPercentage()
            )
        }

    private fun List<PipelineData>.calcSuccessPercentage(): Double {
        val failedCount = count { it.pipeline.status == Status.FAILED }
        val successCount = count { it.pipeline.status == Status.SUCCESS }
        return 100 * successCount.toDouble() / (failedCount + successCount)
    }
}
