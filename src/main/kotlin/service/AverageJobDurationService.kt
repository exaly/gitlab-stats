package service

import data.PipelineRepository
import data.Status

class AverageJobDurationService(
    private val pipelineRepository: PipelineRepository,
    private val jobNames: List<String>
) {

    suspend fun calc() = pipelineRepository.getPipelines()
        .filter { it.pipeline.status == Status.SUCCESS }
        .groupBy { it.yearWeekNumber }
        .map {
            StatsData(
                it.key.year,
                it.key.weekNumber,
                it.value
                    .map { pipeline -> pipeline.jobs.filter { job -> jobNames.contains(job.name) } }
                    .flatten()
                    .mapNotNull { job -> job.duration }
                    .average()
            )
        }
}
