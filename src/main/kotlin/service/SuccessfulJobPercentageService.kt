package service

import data.PipelineJob
import data.PipelineRepository
import data.Status

class SuccessfulJobPercentageService(
    private val pipelineRepository: PipelineRepository,
    private val jobNames: List<String>
) {

    suspend fun calc() = pipelineRepository.getPipelines()
        .filter { it.pipeline.status == Status.SUCCESS }
        .groupBy { it.yearWeekNumber }
        .map {
            StatsData(
                it.key.year,
                it.key.weekNumber,
                it.value
                    .flatMap { pipeline -> pipeline.jobs.filter { job -> jobNames.contains(job.name) } }
                    .calcSuccessPercentage()
            )
        }

    private fun List<PipelineJob>.calcSuccessPercentage(): Double {
        val failedCount = count { it.status == Status.FAILED }
        val successCount = count { it.status == Status.SUCCESS }
        return 100 * successCount.toDouble() / (failedCount + successCount)
    }
}
