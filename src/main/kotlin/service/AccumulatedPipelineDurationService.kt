package service

import data.PipelineRepository

class AccumulatedPipelineDurationService(
    private val pipelineRepository: PipelineRepository
) {

    suspend fun calc() = pipelineRepository.getPipelines()
        .groupBy { it.yearWeekNumber }
        .map {
            StatsData(
                it.key.year,
                it.key.weekNumber,
                it.value
                    .mapNotNull { pipeline -> pipeline.pipeline.duration }
                    .sum()
            )
        }
}
