package service

data class StatsData(
    val year: Int,
    val weekNumber: Int,
    val value: Double
)
