package client

import data.Pipeline
import data.PipelineDetail
import data.PipelineJob
import network.SimpleNetworker
import java.net.URI

class GitlabClient(
    private val simpleNetworker: SimpleNetworker,
    rootLink: URI,
    accessToken: String,
    projectId: String
) {

    private val pipelineEndpoint = "/projects/$projectId/pipelines"

    init {
        simpleNetworker.apply {
            basePath = rootLink
            baseHeaders = mapOf("PRIVATE-TOKEN" to accessToken)
        }
    }

    suspend fun getPipelines(): List<Pipeline> =
        simpleNetworker.getFullPaginatedList(pipelineEndpoint, "per_page=100&page")

    suspend fun getPipelineDetail(id: Int): PipelineDetail =
        simpleNetworker.getObject("$pipelineEndpoint/$id")

    suspend fun getPipelineJobs(id: Int): List<PipelineJob> =
        simpleNetworker.getList("$pipelineEndpoint/$id/jobs")
}
