import service.*

class Printer(
    private val averageJobDurationService: AverageJobDurationService,
    private val accumulatedPipelineDurationService: AccumulatedPipelineDurationService,
    private val successfulJobPercentageService: SuccessfulJobPercentageService,
    private val successfulPipelinesPercentageService: SuccessfulPipelinesPercentageService
) {

    suspend fun print() {
        print("AVG. JOB DURATION PER / WEEK") {
            averageJobDurationService.calc().print()
        }

        print("ACC. PIPELINE DURATION / WEEK") {
            accumulatedPipelineDurationService.calc().print()
        }

        print("SUCCESSFUL JOB PERC. (ON SUCCESSFUL PIPELINE) / WEEK") {
            successfulJobPercentageService.calc().print()
        }

        print("SUCCESSFUL PIPELINES PERC. / WEEK") {
            successfulPipelinesPercentageService.calc().print()
        }
    }

    private suspend fun print(title: String, action: suspend () -> Unit) {
        println("===========================================")
        println("[[[[ $title ]]]]")
        println("===========================================")
        action.invoke()
        println("===========================================")
    }

    private fun List<StatsData>.print() =
        forEach { println("${it.year},${it.weekNumber},${it.value}") }
}
