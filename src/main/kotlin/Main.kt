import network.ResponsePrinter
import client.GitlabClient
import com.github.kittinunf.fuel.core.FuelManager
import data.PipelineCache
import kotlinx.coroutines.runBlocking
import data.PipelineRepository
import network.SimpleNetworker
import org.koin.core.context.startKoin
import org.koin.dsl.module
import service.AccumulatedPipelineDurationService
import service.AverageJobDurationService
import service.SuccessfulJobPercentageService
import service.SuccessfulPipelinesPercentageService
import java.net.URI

fun main(vararg args: String) {
    if (args.size < 4) throw IllegalArgumentException(
        "Missing arguments:\n" +
                "1. Gitlab root link\n" +
                "2. Authentication key\n" +
                "3. Project ID\n" +
                "4. Job name(s)"
    )
    val gitlabRootLink = URI.create(args[0])
    val gitlabAccessToken = args[1]
    val projectId = args[2]
    val jobNames = args.asList().subList(3, args.lastIndex)

    val clientModule = module {
        single { FuelManager.instance }
        single { ResponsePrinter() }
        single { SimpleNetworker(get(), get()) }
        single { GitlabClient(get(), gitlabRootLink, gitlabAccessToken, projectId) }
    }

    val dataModule = module {
        single { PipelineCache() }
        factory { PipelineRepository(get(), get()) }
    }

    val serviceModule = module {
        factory { AverageJobDurationService(get(), jobNames) }
        factory { AccumulatedPipelineDurationService(get()) }
        factory { SuccessfulJobPercentageService(get(), jobNames) }
        factory { SuccessfulPipelinesPercentageService(get()) }
        factory { Printer(get(), get(), get(), get()) }
    }

    startKoin {
        modules(clientModule, dataModule, serviceModule)
    }

    runBlocking {
        App().print()
    }
}
