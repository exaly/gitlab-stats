import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class App : KoinComponent {
    private val printer by inject<Printer>()

    suspend fun print() = printer.print()
}