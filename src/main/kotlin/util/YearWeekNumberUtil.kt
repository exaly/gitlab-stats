package util

import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import data.YearWeekNumber
import java.time.temporal.WeekFields

fun Instant.toYearWeekNumber() = toLocalDateTime(TimeZone.UTC)
    .toJavaLocalDateTime()
    .toLocalDate()
    .let { YearWeekNumber(it.year, it.get(WeekFields.ISO.weekOfWeekBasedYear())) }
