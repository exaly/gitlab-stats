package data

import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PipelineDetail(
    val id: Int,
    val status: Status = Status.UNKNOWN,
    @SerialName("created_at") val createdAt: Instant,
    val duration: Double?
)
