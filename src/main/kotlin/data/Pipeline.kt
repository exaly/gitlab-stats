package data

import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Pipeline(
    val id: Int,
    val status: Status = Status.UNKNOWN,
    @SerialName("created_at") val createdAt: Instant
)
