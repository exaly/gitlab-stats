package data

data class YearWeekNumber(
    val year: Int,
    val weekNumber: Int
)
