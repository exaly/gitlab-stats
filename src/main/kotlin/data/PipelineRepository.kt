package data

import client.GitlabClient
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import util.pmap
import util.toYearWeekNumber

class PipelineRepository(
    private val gitlabClient: GitlabClient,
    private val pipelineCache: PipelineCache
) {

    suspend fun getPipelines(): List<PipelineData> = pipelineCache.cache ?: gitlabClient.getPipelines()
        .pmap { getPipelineData(it.id) }
        .also { pipelineCache.cache = it }

    private suspend fun getPipelineData(id: Int): PipelineData = coroutineScope {
        val detailJob = async { gitlabClient.getPipelineDetail(id) }
        val jobsJob = async { gitlabClient.getPipelineJobs(id) }
        val detail = detailJob.await()
        PipelineData(detail.createdAt.toYearWeekNumber(), detail, jobsJob.await())
    }
}
