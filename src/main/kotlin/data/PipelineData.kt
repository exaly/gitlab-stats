package data

data class PipelineData(
    val yearWeekNumber: YearWeekNumber,
    val pipeline: PipelineDetail,
    val jobs: List<PipelineJob>
)
