package data

import kotlinx.serialization.Serializable

@Serializable
data class PipelineJob(
    val id: Int,
    val name: String,
    val duration: Double?,
    val status: Status = Status.UNKNOWN
)
