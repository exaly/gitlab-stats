package data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class Status {
    @SerialName("success") SUCCESS,
    @SerialName("failed") FAILED,
    UNKNOWN
}