# GitLab Stats

Supports the following features:

1. Calculate average job duration per week
2. Percentage successful pipeline runs per week
3. Accumulated pipeline duration per week

These metrics can be used to analyse a project hosted on GitLab.


### How to use

```
gitlab-stats.jar [GitLab root link] [Access token] [Project ID] *[Job names]
```